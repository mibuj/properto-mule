Properto-CRM Integration project version: 0.0.1

Instalacja serwera:
*Java 8
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
$ sudo adduser mule
$ sudo usermod -s /bin/bash -d /home/mule -a -G sudo mule 
// IN /home/mule as user mule
$ wget https://repository-master.mulesoft.org/nexus/content/repositories/releases/org/mule/distributions/mule-standalone/3.8.0/mule-standalone-3.8.0.tar.gz
$ sudo tar -xf mule-standalone-3.8.0.tar.gz -C /opt/
$ sudo chown -R mule /opt/mule-standalone-3.8.0
$ sudo chmod -R u+rX /opt/mule-standalone-3.8.0

Linux/Unix:
    $ export MULE_HOME=/opt/mule-standalone-3.8.0
    $ export PATH=$PATH:$MULE_HOME/bin

//read before install activemq: https://hostpresto.com/community/tutorials/how-to-install-apache-activemq-on-ubuntu-14-04/

$ sudo useradd activemq
$ sudo passwd activemq
$ mkdir /home/activemq
$ chown activemq.activemq /home/activemq
$ sudo su activemq
$ sudo wget https://archive.apache.org/dist/activemq/5.14.4/apache-activemq-5.14.4-bin.tar.gz
$ gunzip apache-activemq-5.14.4-bin.tar.gz
$ tar -xvf apache-activemq-5.14.4-bin.tar
$ sudo mv /home/activemq/apache-activemq-5.14.4/ /opt
$ sudo ln -s /opt/apache-activemq-5.14.4/bin/linux-x86-64/activemq /etc/init.d/activemq
//modify the activemq script in /opt/apache-activemq-5.11.1/bin/linux-x86-64, set RUN_AS_USER=activemq
$ sudo apt-get install sysv-rc-conf
$ sudo mkdir /var/log/activemq
$ sudo chown activemq.activemq /var/log/activemq/
$ sudo chmod 0755 /var/log/activemq/

// Add mule and activemq systemctl
//ACTIVEMQ start script
[Unit]
Description=Apache ActiveMQ
After=network-online.target

[Service]
Type=forking
WorkingDirectory=/opt/apache-activemq-5.14.3/bin
ExecStart=/opt/apache-activemq-5.14.3/bin/activemq start
ExecStop=/opt/apache-activemq-5.14.3/bin/activemq stop
Restart=on-abort
User=activemq
Group=activemq

[Install]
WantedBy=multi-user.target

//MULE start script
[Unit]
Description=Mule ESB
After=network.target

[Service]
Type=forking
User=mule
Group=mule

ExecStart=/opt/mule-standalone-3.8.0/bin/mule start
ExecStop=/opt/mule-standalone-3.8.0/bin/mule stop
ExecReload=/opt/mule-standalone-3.8.0/bin/mule restart

#TimeoutSec=300

[Install]
WantedBy=multi-user.target


$ sudo apt-get install nginx
//nginx.conf in /etc/nginx/

apt-get install vsftpd 
addgroup ftpgroup
adduser ftpuser -shell /bin/false -home /ftp
password: ftp
adduser ftpuser ftpgroup

GET Cert
sudo echo -n | openssl s_client -connect 85.232.230.143:8081/upload | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /mennica.txt