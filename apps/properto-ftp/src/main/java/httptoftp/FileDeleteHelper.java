package httptoftp;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class FileDeleteHelper implements Callable {

	final String FTP_ADDRESS = "ftp.address";
	final String PORT = "ftp.port";
	final String USER = "ftp.user";
	final String PASSWORD = "ftp.password";
	final String ADDRESS = "ftp.static_address";

	Properties prop;
	InputStream input = null;

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		String filedel = (String) eventContext.getMessage().getProperty(
				"contentVar", PropertyScope.SESSION);
		eventContext.getMessage().setProperty("delFile",
				filedel, PropertyScope.INVOCATION);

	return eventContext.getMessage().getPayload();
		
	}

}
