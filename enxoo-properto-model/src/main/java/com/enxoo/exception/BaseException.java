package com.enxoo.exception;

import java.util.ArrayList;
import java.util.List;

import com.enxoo.response.BaseResponse;

public class BaseException extends BaseResponse {
	int statusCode;
	String message;

	
	public BaseException(int statusCode, String message){
		this.statusCode = statusCode;
		this.message = message;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
