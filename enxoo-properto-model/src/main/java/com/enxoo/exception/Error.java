package com.enxoo.exception;

import javax.validation.ConstraintViolation;

import com.enxoo.model.BaseMessage;

public class Error {
	public Error(ConstraintViolation<BaseMessage> cv) {
		this.message = cv.getMessage();
		this.field = cv.getPropertyPath().toString();
	}

	String message;
	String field;
	
	public Error(String message, String field)
	{
		this.message = message;
		this.field = field;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
}
