package com.enxoo.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import com.enxoo.model.BaseMessage;

public class ValidationException extends BaseException{
	
	List<Error> errors = new ArrayList<Error>();
	
	public ValidationException(Set<ConstraintViolation<BaseMessage>> constraints){
		super(400, "Validation exception");
		for(ConstraintViolation<BaseMessage> cv: constraints){
			this.errors.add(new Error(cv));
		}
	}
	
	public List<Error> getErrors() {
		return errors;
	}
	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

}
