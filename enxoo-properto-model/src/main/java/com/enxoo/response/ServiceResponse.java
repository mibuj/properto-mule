package com.enxoo.response;

public class ServiceResponse extends BaseResponse {
	int statusCode;
	String message;
	
	public ServiceResponse(int statusCode, String message){
		this.statusCode = statusCode;
		this.message = message;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
