package com.enxoo.swagger;

import io.swagger.models.Contact;
import io.swagger.models.Info;
import io.swagger.models.License;
import io.swagger.models.Swagger;
import io.swagger.models.Tag;
import io.swagger.mule.ApiListingJSON;


public class Bootstrap {
	/** Run on app init by Spring */
	public void start(){
	    Info info = new Info()
	    .title("Swagger Sample App")
	      .contact(new Contact().email("kamil.baluc@enxoo.com"))
	      .license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html"));

	    Swagger swagger = new Swagger().info(info);
	    swagger.tag(new Tag().name("lead").description("Webpage Services"));
	    ApiListingJSON.init(swagger);
	  }
	}